#include "logger.h"

#include <ctime>
#include <chrono>
#include <iomanip>
#include <sstream>

Logger::Logger()
{
    m_hLogMutex = CreateMutex(  NULL,
                                FALSE,
                                NULL );
}
Logger::~Logger()
{
    ReleaseMutex( m_hLogMutex );
    CloseHandle( m_hLogMutex );
}

std::string Logger::getSeverityString( ELogSeverity severity ) const
{
    std::string result = "NONE";

    auto itSeverity = m_severity_strings.find( severity );
    if ( itSeverity != m_severity_strings.end() )
    {
        result = itSeverity->second;
    }

    return result;
}

std::string Logger::getTimeString() const
{
    auto now = std::chrono::system_clock::now();
    auto ms = std::chrono::duration_cast<std::chrono::milliseconds>( now.time_since_epoch() ) % 1000;

    struct tm currentTM;
    time_t nowt = time( 0 );
    localtime_s( &currentTM, &nowt );

    std::stringstream ss;
    ss << std::put_time( &currentTM, "%F %T" );
    ss << '.' << std::setfill( '0' ) << std::setw( 3 ) << ms.count();

    return ss.str();
}

void Logger::log( ELogSeverity severity, const std::string& fileName, const char* message )
{
    WaitForSingleObject(m_hLogMutex, INFINITE);

    m_file.open( "log.txt", std::ios::app );
    if ( !m_file.is_open() )
    {
        ReleaseMutex( m_hLogMutex );
        return;
    }

    m_file << getTimeString() 
        << " "
        << fileName
        << " "
        << getSeverityString( severity )
        << " " 
        << message
        << std::endl;


    m_file.close();

    ReleaseMutex( m_hLogMutex );
}