#ifndef _LOGGER_HPP_
#define _LOGGER_HPP_

#include <windows.h>
#include <string>
#include <fstream>
#include <map>

class Logger
{
public:

	enum class ELogSeverity
	{
		eLS_info,
		eLS_warning,
		eLS_error
	};

	static Logger* getInstance()
	{
		static Logger singleLogger;
		return &singleLogger;
	}

	void log(ELogSeverity severity, const std::string& fileName, const char* message);

private:

	Logger();
	~Logger();

	Logger ( const Logger & );
	Logger & operator = ( const Logger & );


private:

	std::string getSeverityString( ELogSeverity severity ) const;

	std::string getTimeString() const;

private:

	std::fstream m_file;

	HANDLE m_hLogMutex;

	const std::map<ELogSeverity, std::string> m_severity_strings
	{
		{ ELogSeverity::eLS_error, "ERROR"},
		{ ELogSeverity::eLS_warning, "WARNING"},
		{ ELogSeverity::eLS_info, "INFO"},
	};
};

#endif //_LOGGER_HPP_