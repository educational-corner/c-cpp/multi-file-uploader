#include "base_win.h"
#include "main_window.h"
#include <atlstr.h>
#include <iostream>

int WINAPI wWinMain( HINSTANCE hInstance, HINSTANCE, PWSTR pCmdLine, int nCmdShow )
{
  
    LPWSTR* argv;
    int argc;

    argv = CommandLineToArgvW( GetCommandLineW(), &argc );

    MainWindow win;

    if ( argv )
    {
        for ( int i = 1; i < argc; i++ )
        {                
            win.addAddress( argv[i] );
        }
    }

    if ( !win.Create( L"Main Window", WS_OVERLAPPEDWINDOW ) )
    {
        return 0;
    }

    ShowWindow( win.Window(), nCmdShow );

    // Run the message loop.

    MSG msg = { };
    while ( GetMessage( &msg, NULL, 0, 0 ) )
    {
        TranslateMessage( &msg );
        DispatchMessage( &msg );
    }

    return 0;
}
