#include "main_window.h"
#include "../res/Resource.h"

#pragma comment( lib, "winhttp.lib" )
#pragma comment( lib, "ComCtl32.Lib" )

#include <vector>
#include <fstream>
#include <cassert>
#include <shlobj.h>
#include <strsafe.h>
#include <algorithm>


MainWindow::~MainWindow()
{
    //Close Handles
    if ( m_phThreads )
    {
        for ( int i = 0; i < m_addresses.size(); i++ )
        {
            CloseHandle( m_phThreads[i] );
        }

        delete[] m_phThreads;
    }

    for ( auto it : m_thParams )
    {
        delete it;
        it = nullptr;
    }
}

LRESULT MainWindow::HandleMessage( UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    switch ( uMsg )
    {
    case WM_CREATE:
    {
        m_logger = Logger::getInstance();
        makeUniqueAdresses();
        addWidgets();
        m_hMutex = CreateMutex( NULL, FALSE, NULL );
        m_threadID = GetCurrentThreadId();
    }
    break;
    case WM_COMMAND:
    {
        if ( wParam == BUTTON_SAVE_FILE )
        {
            
            setPathName();

            int handleSize = m_addresses.size();
            m_phThreads = new HANDLE[handleSize];

            for ( int i = 0; i < m_addresses.size(); i++ )
            {
                ThreadParams* pthParameter = new ThreadParams;
                pthParameter->pMainWin = this;
                pthParameter->index = i;

                m_thParams.push_back( pthParameter );

                m_phThreads[i] = CreateThread(NULL,
                                                0,
                                                sFileLoader,
                                                ( LPVOID )pthParameter,
                                                0,
                                                NULL );

                //loadFile(i);
            }
        }
    }
    break;
    case CHANGE_STATUS:
    {
        changeStatus( ( EStatus )wParam, lParam );
    }
    break;
    case WM_DESTROY:
    {
        CloseHandle( m_hMutex );
        PostQuitMessage( 0 );
    }
    return 0;

    case WM_PAINT:
    {
        PAINTSTRUCT ps;
        HDC hdc = BeginPaint( m_hwnd, &ps );
        FillRect( hdc, &ps.rcPaint, ( HBRUSH )( COLOR_WINDOW ) );
        EndPaint( m_hwnd, &ps );
    }
    return 0;

    default:
        return DefWindowProc( m_hwnd, uMsg, wParam, lParam );
    }
    return TRUE;
}


void MainWindow::loadFile( int index )
{
    PostMessage(m_hwnd, CHANGE_STATUS, ( WPARAM )EStatus::eS_Downloading, index );

    bool downloadIsFailed = false;
    
    std::wstring fullAdress = m_addresses[index];

    std::wstring fullPathW;
    std::wstring siteNameW;
    std::wstring fileNameW;
    std::wstring fileFilterW;

    if ( !stringSlice( fullAdress, fullPathW, siteNameW, fileNameW ) )
    {
        std::wstring failedMessage = L"Invalid input format";
        changeDownloadingStatus( index, failedMessage );

        PostMessage( m_hwnd, CHANGE_STATUS, ( WPARAM )EStatus::eS_Failed, index );

        std::string fName = std::string( m_addresses[index].begin(), m_addresses[index].end() );

        m_logger->log( Logger::ELogSeverity::eLS_error, fName, "Invalid input format" );

        return;
    }
    std::string fileName = std::string( fileNameW.begin(), fileNameW.end() );

    m_logger->log( Logger::ELogSeverity::eLS_info, fileName, "Trying to download file" );

    // For downloading process
    DWORD sizeInc = 0;
    DWORD dwFullSize = 0;
    DWORD sizeD = sizeof( DWORD );

    DWORD dwSize = 0;
    DWORD dwDownloaded = 0;
    LPSTR pszOutBuffer;
    BOOL  bResults = FALSE;

    HINTERNET hSession = NULL;
    HINTERNET hConnect = NULL;
    HINTERNET hRequest = NULL;

    // Use WinHttpOpen to obtain a session handle.
    hSession = WinHttpOpen( L"WinHTTP Example/1.0",
                            WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
                            WINHTTP_NO_PROXY_NAME,
                            WINHTTP_NO_PROXY_BYPASS, 0 );

    // Specify an HTTP server.
    if (hSession)
    {
        hConnect = WinHttpConnect(hSession, siteNameW.c_str(),
            INTERNET_DEFAULT_PORT, 0);
    }

    // Create an HTTP request handle.
    if (hConnect)
    {
        hRequest = WinHttpOpenRequest(hConnect, L"GET", fullPathW.c_str(),
            NULL, WINHTTP_NO_REFERER,
            WINHTTP_DEFAULT_ACCEPT_TYPES,
            NULL);
    }

    // Send a request.
    if ( hRequest )
    {
        bResults = WinHttpSendRequest(  hRequest,
                                        WINHTTP_NO_ADDITIONAL_HEADERS,
                                        0, WINHTTP_NO_REQUEST_DATA, 0,
                                        0, 0 );
    }

    // End the request.
    if ( bResults )
    {
        bResults = WinHttpReceiveResponse( hRequest, NULL );
    }

    if ( bResults )
    {
        unsigned long dwStatusCode = 0;
        unsigned long dwSize = sizeof( dwStatusCode );
        

        WinHttpQueryHeaders( hRequest,
                            WINHTTP_QUERY_STATUS_CODE | WINHTTP_QUERY_FLAG_NUMBER,
                            WINHTTP_HEADER_NAME_BY_INDEX,
                            &dwStatusCode, &dwSize,
                            WINHTTP_NO_HEADER_INDEX );

        if ( dwStatusCode == 404 )
        {
            downloadIsFailed = true;
            bResults = NULL;
            std::wstring failedMessage = L"Error 404, file doesn't exsist";
            changeDownloadingStatus(index, failedMessage);
            m_logger->log(Logger::ELogSeverity::eLS_error, fileName, "Error 404, file doesn't exsist");
        }
    }

    if ( bResults )
    {
        // Get the full size of the file
        WinHttpQueryHeaders( hRequest,
                            WINHTTP_QUERY_CONTENT_LENGTH | WINHTTP_QUERY_FLAG_NUMBER,
                            NULL, &dwFullSize, &sizeD,
                            WINHTTP_NO_HEADER_INDEX );

        char pathToSave[260];
        size_t charsConverted = 0;
        wcstombs_s( &charsConverted,
                    pathToSave,
                    sizeof( pathToSave ),
                    m_pathName, wcslen( m_pathName ) + 1 );

        std::string pathToSaveStr;
        if ( m_pathIsSet )
        {
            pathToSaveStr = pathToSave;
            pathToSaveStr += "/";
        }
        pathToSaveStr += fileName;

        FILE* pFile; // NEW

        fopen_s( &pFile, pathToSaveStr.c_str(), "w+b" ); // NEW

        // Keep checking for data until there is nothing left.
        if ( pFile )
            do
            {
                // Check for available data.
                dwSize = 0;
                if ( !WinHttpQueryDataAvailable( hRequest, &dwSize ) )
                {
                    downloadIsFailed = true;

                    m_logger->log( Logger::ELogSeverity::eLS_error, fileName, "Error in WinHttpQueryDataAvailable" );

                }

                sizeInc+= dwSize;
                
                std::wstring processDownload = setDownloadingBar( sizeInc, dwFullSize );
                changeDownloadingStatus( index, processDownload );

                processDownload.clear();
                
                // Allocate space for the buffer.
                pszOutBuffer = new char[dwSize + 1];
                    if ( !pszOutBuffer )
                {
                    downloadIsFailed = true;
                    printf( "Out of memory\n" );
                    dwSize = 0;
                }
                else
                {
                    // Read the Data.
                    ZeroMemory( pszOutBuffer, dwSize + 1 );

                    if ( !WinHttpReadData(  hRequest,
                                            ( LPVOID )pszOutBuffer,
                                            dwSize,
                                            &dwDownloaded ) )
                    {
                        downloadIsFailed = true;

                        m_logger->log( Logger::ELogSeverity::eLS_error, fileName, "Error in WinHttpReadData" );                      
                    }
                    else
                    {
                        printf( "%s", pszOutBuffer );
                        // Data in vFileContent
                        fwrite( pszOutBuffer, ( size_t )dwDownloaded, ( size_t )1, pFile );
                    }

                    // Free the memory allocated to the buffer.
                    delete[] pszOutBuffer;
                }

            } while ( dwSize > 0 );

            fclose( pFile );

            if ( downloadIsFailed )
            {
                PostMessage( m_hwnd, CHANGE_STATUS, ( WPARAM )EStatus::eS_Failed, index );

                m_logger->log( Logger::ELogSeverity::eLS_error, fileName, "Downloading failed" );
            }
            else
            {
                PostMessage( m_hwnd, CHANGE_STATUS, ( WPARAM )EStatus::eS_Downloaded, index );

                m_logger->log( Logger::ELogSeverity::eLS_info, fileName, "Downloading succeed" );
            }
    }

    // Report any errors.
    if ( !bResults )
    {
        
        std::wstring failedMessage = L"Error has occurred";

        PostMessage( m_hwnd, CHANGE_STATUS, (WPARAM)EStatus::eS_Failed, index );

        m_logger->log( Logger::ELogSeverity::eLS_error, fileName, "Error has occurred" );
    }

    // Close any open handles.
    if ( hRequest ) WinHttpCloseHandle( hRequest );
    if ( hConnect ) WinHttpCloseHandle( hConnect );
    if ( hSession ) WinHttpCloseHandle( hSession );

}

void MainWindow::addWidgets()
{
    GetClientRect( m_hwnd, &m_clRect );

    addListView();

    CreateWindowW(  L"BUTTON",
                    L"Download Files",
                    WS_VISIBLE | WS_CHILD,
                    ( m_clRect.right - m_clRect.left ) / 2 - 100, m_clRect.bottom - m_clRect.top - 50,
                    200, 50,
                    m_hwnd,
                    ( HMENU )BUTTON_SAVE_FILE,
                    NULL,
                    NULL );
}
bool  MainWindow::stringSlice(  const std::wstring& fullAdress,
                                std::wstring& fullPath,
                                std::wstring& siteName,
                                std::wstring& fileName )
{
    // Cut off "http://" or "https://"
    fullPath = fullAdress;
    auto endOfProtocolPosition = fullPath.find_first_of( '/' );
    
    if( endOfProtocolPosition != std::string::npos )
        fullPath.erase( 0, endOfProtocolPosition + 2 );
    else
    {
        // Invalid format input
        return false;
    }

    // From the siteName cutting off the path
    siteName = fullPath;
    auto endOfSiteNamePosition = siteName.find_first_of( '/' );

    if( endOfSiteNamePosition != std::string::npos )
        siteName.erase( endOfSiteNamePosition,
                        std::numeric_limits< std::string::size_type >::max() );

    // Making fullPath without siteName
    auto endOfSitePos = fullPath.find_first_of( '/' );

    if ( endOfSitePos != std::string::npos )
        fullPath.erase( 0, endOfSitePos );
    else
    {
        // Invalid format input
        return false;
    }

    // From the fileName cutting off everything forward
    fileName = fullPath;
    auto position = fileName.find_last_of( '/' );

    if ( position != std::string::npos )
        fileName.erase( 0, position + 1 );

    // If the file name doesn't have '.' - invalid format

    auto beginningOfFormatPos = fileName.find_last_of( '.' );

    if ( beginningOfFormatPos == std::string::npos )
    {
        // Invalid format input
        return false;
    }

    // If the file name have one of these symbols - invalid format

    wchar_t invalidSymbols[] = L"%+?=&";

    const wchar_t* pch;
    pch = wcspbrk( fileName.c_str(), invalidSymbols );

    if (pch)
    {
        // Invalid format input
        return false;
    }
    


    return true;
}

DWORD WINAPI MainWindow::sFileLoader( LPVOID lParam )
{
    ThreadParams* thPar = ( ThreadParams* )lParam;
    int index = thPar->index;
    return ( thPar->pMainWin )->threadFileLoader( ( LPVOID )index );
}

DWORD WINAPI MainWindow::threadFileLoader( LPVOID lParam )
{
    int index = ( int )lParam;

    loadFile( index );

    return 0;
}


void MainWindow::addAddress( const wchar_t* address )
{
    m_addresses.push_back( address );
}

void MainWindow::addListView()
{

    createListView();

    addColumns();

    for ( int i = 0; i < m_addresses.size(); i++ )
    {
        addListViewItems( i );
    }
    
}


void MainWindow::createListView()
{
    m_hwndLV = CreateWindow( WC_LISTVIEW,
                            L"",
                            WS_VISIBLE | WS_BORDER | WS_CHILD | LVS_REPORT | LVS_EDITLABELS,
                            0,
                            0,
                            m_clRect.right - m_clRect.left,
                            m_clRect.bottom - m_clRect.top - 50,
                            m_hwnd,
                            NULL, NULL, 0 );

    ListView_SetExtendedListViewStyleEx( m_hwndLV, 0, LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES );
}
void MainWindow::addColumns()
{
    LVCOLUMN lvc;
    lvc.mask = LVCF_TEXT | LVCF_WIDTH;
    GetClientRect( m_hwnd, &m_clRect );
    // Depends on how many colnums we want to add, now its 3 == "/ 3"
    lvc.cx = ( m_clRect.right - m_clRect.left ) / 3;
    lvc.cchTextMax = 260;

    addColnum( lvc, L"File to Download : ", 0 );
    addColnum( lvc, L"Status : ", 1 );
    addColnum( lvc, L"Progress : ", 2 );
}

void MainWindow::addColnum( LVCOLUMN &lvc, const wchar_t* colnumName, int position )
{
    lvc.pszText = const_cast<LPWSTR>( colnumName );
    ListView_InsertColumn( m_hwndLV, position, &lvc );
}

void MainWindow::addListViewItems( int nameIndex )
{
    int iLastIndex = ListView_GetItemCount( m_hwndLV );

    LVITEM lvi;
    lvi.mask = LVIF_TEXT;
    lvi.cchTextMax = 260;
    lvi.iItem = iLastIndex;

    lvi.pszText = const_cast<LPWSTR>( m_addresses[nameIndex].c_str() );
    lvi.iSubItem = 0;

    ListView_InsertItem( m_hwndLV, &lvi );

    lvi.iSubItem = 1;

    lvi.pszText = const_cast<LPWSTR>( L"Ready to download" );
    ListView_SetItem( m_hwndLV, &lvi );
}


void MainWindow::changeStatus( EStatus status, int nameIndex )
{
    auto itStatus = m_status_strings.find( status );
    if ( itStatus != m_status_strings.end() )
    {
        std::wstring _wstring = itStatus->second;
        LPWSTR ptrStatus = &_wstring[0];
        ListView_SetItemText( m_hwndLV, nameIndex, 1, ptrStatus );
    }
}

void MainWindow::changeDownloadingStatus( int nameIndex, const std::wstring& processDownload )
{
    ListView_SetItemText( m_hwndLV, nameIndex, 2, const_cast<LPWSTR>( processDownload.c_str() ) );
}

void MainWindow::setPathName()
{
    BROWSEINFO brwinfo = { 0 };
    brwinfo.lpszTitle = L"Select Your Source Directory";
    brwinfo.hwndOwner = m_hwnd;
    LPITEMIDLIST pitemidl = SHBrowseForFolder( &brwinfo );

    if (pitemidl == 0)
    {
        return;
    }

    // get the full path of the folder
    TCHAR path[MAX_PATH];
    if (SHGetPathFromIDList( pitemidl, path) )
    {
        m_pathIsSet = true;
        StringCchCopy( m_pathName, MAX_PATH, path );
    }

    IMalloc* pMalloc = 0;
    if ( SUCCEEDED( SHGetMalloc( &pMalloc ) ) )
    {
        pMalloc->Free( pitemidl );
        pMalloc->Release();
    }

}

std::wstring MainWindow::setDownloadingBar( int downloaded, int fullSize )
{
    std::wstring processDownload;
    processDownload += std::to_wstring( downloaded );
    processDownload += L" / ";
    processDownload += std::to_wstring( fullSize );
    processDownload += L" bytes";

    return processDownload;
}

void MainWindow::makeUniqueAdresses()
{
    std::sort( m_addresses.begin(), m_addresses.end() );

    auto last = std::unique( m_addresses.begin(), m_addresses.end() );
    m_addresses.erase( last, m_addresses.end() );
}