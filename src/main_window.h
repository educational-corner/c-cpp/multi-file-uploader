#ifndef _MAIN_WINDOW_H_
#define _MAIN_WINDOW_H_

#include "base_win.h"
#include "logger.h"

#include <string>
#include <vector>
#include <map>
#include <winhttp.h>
#include <CommCtrl.h>

class MainWindow : public BaseWindow<MainWindow>
{
public:

    struct ThreadParams
    {
        MainWindow* pMainWin;
        int index;

        ~ThreadParams()
        {
            pMainWin = nullptr;
        }
    };

public:

    ~MainWindow();

    PCWSTR  ClassName() const { return L"Main Window Class"; }
    LRESULT HandleMessage( UINT uMsg, WPARAM wParam, LPARAM lParam );

    void addAddress( const wchar_t* adress );

private:

    RECT m_clRect;

    void addWidgets();

private:

    enum EStatus
    {
        eS_Downloading,
        eS_Failed,
        eS_Downloaded
    };

    HWND m_hwndLV;

    void addListView();
    void createListView();
    void addColumns();
    void addColnum( LVCOLUMN& lvc, const wchar_t* colnumName, int position );
    void addListViewItems( int nameIndex );
    void changeStatus( EStatus status, int nameIndex );
    void changeDownloadingStatus( int nameIndex, const std::wstring & processDownload );
    std::wstring setDownloadingBar( int downloaded, int fullSize );

    const std::map<EStatus, std::wstring> m_status_strings
    {
        { EStatus::eS_Downloading, L"Downloading" },
        { EStatus::eS_Failed, L"Failed" },
        { EStatus::eS_Downloaded, L"Downloaded" }
    };

private:

    void setPathName();
    bool m_pathIsSet = false;

    void loadFile(int index);
    bool stringSlice( const std::wstring& fullAdress,
                        std::wstring& fullPath,
                        std::wstring& siteName,
                        std::wstring& fileName );

    static DWORD WINAPI sFileLoader( LPVOID lParam );

    DWORD WINAPI threadFileLoader( LPVOID lParam );

    std::vector < ThreadParams* > m_thParams;

private:

    std::vector< std::wstring > m_addresses;

    void makeUniqueAdresses();

    HANDLE* m_phThreads;

    int m_threadID;

private:

    TCHAR m_pathName[260];
    
private:

    Logger* m_logger;

private:

    HANDLE m_hMutex;
};

#endif //_MAIN_WINDOW_H_
